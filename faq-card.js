const buttons = Array.from(document.querySelectorAll('.faq-button'));

function toggleFaqAnswer(event) {
    const currentLi = event.currentTarget.parentElement || event.target.parentElement;
    currentLi.querySelector('.question').classList.toggle('bold');
    currentLi.querySelector('.answer').classList.toggle('hidden');
    currentLi.querySelector('.button-icon').classList.toggle('inverted');
    currentLi.querySelector('.faq-button').classList.toggle('no-focus');
}

function handleKeyEvent(event) {
        if(event.key === 'Enter') toggleFaqAnswer(event);
}

buttons.forEach(b => b.addEventListener('click', toggleFaqAnswer));
window.addEventListener('keyup', handleKeyEvent);